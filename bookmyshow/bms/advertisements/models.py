# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class User(models.Model):
    username = models.CharField(max_length=200,null=True)
    language = models.CharField(max_length=50,null=True)
    location = models.CharField(max_length=50,null=True)
    hasbooked = models.IntegerField(null=True)
    gender = models.CharField(max_length=1,null=True)

class Advertisement(models.Model):
    name = models.CharField(max_length=100,null=True)
    description = models.CharField(max_length=600, null=True)
    type = models.CharField(max_length=100, null=True)
    gender = models.CharField(max_length=1, null=True)
    language = models.CharField(max_length=70, null=True)
    link = models.CharField(max_length=2500, null=True)


class Movie(models.Model):
    name = models.CharField(max_length=800, null=True)
    genres = models.CharField(max_length=300,null=True)
    language = models.CharField(max_length=300,null=True)

