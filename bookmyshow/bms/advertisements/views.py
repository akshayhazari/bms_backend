# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
import nmslib, json, csv, pickle, pandas,os
from collections import defaultdict, Counter
# Create your views here.

from models import User,Movie
from rest_framework.response import Response

from rest_framework.views import APIView
from django.conf import settings

class getRecommendations(APIView):

    def get_random(self,n, p):
        return str(numpy.random.choice(numpy.arange(0, n), p=p))

    def get_recommendation(self,rec):
        index = nmslib.init(method='hnsw', space='cosinesimil')
        index.loadIndex(os.path.join(settings.BASE_DIR, "pickle/knn_rec.hnsw"))

        ln = ['Telugu', 'Marathi', 'Punjabi', 'Hindi', 'Tamil', 'Bengali']
        gn = ['action', 'drama', 'adventure', 'romantic', 'animation', 'comedy', 'musical', 'thriller', 'biography',
            'fatasy', 'horror', 'mystery']
        names = ['username', 'count'] + ln + ['language', 'gender', 'hasbooked']+gn
        names_d = {names[i]: i for i in range(9, len(names))}
        language = rec['language'].title()
        lang = [0]*len(ln)
        if language in ln:
            lang[ln.index(language)] = 1
        genres=rec['genres'].split(',')
        gen = [0]* len(gn)
        for i in genres:
            if i in gn:
                gen[gn.index(i)] = 1
        gender = 1 if rec['gender'] =="m" else 0
        d = [rec['username'],1]+lang+[language,gender, rec['hasbooked']]+gen
        data = pandas.read_csv(os.path.join(settings.BASE_DIR, 'reviews_merged_parsed_aggr.txt'), sep='~', skiprows=[0],
                               usecols=[i for i in range(0, 8)] + [i for i in range(9, len(names))],
                               names=names[0:8] + names[9:],
                               na_filter=False)
        neighbours, distances = index.knnQuery(d[1:8] + d[9:], k=10)

        user_mov_d = pickle.load(open(os.path.join(settings.BASE_DIR, 'pickle/user_mov.p'), 'rb'))
        user_hasbooked = pickle.load(open(os.path.join(settings.BASE_DIR, 'pickle/user_hasbooked.p'), 'rb'))
        neighbour_mov_cnt = pickle.load(open(os.path.join(settings.BASE_DIR, 'pickle/neighbour_mov_cnt.p'), 'rb'))
        advertisement = pickle.load(open(os.path.join(settings.BASE_DIR, 'pickle/advertisement.p'), 'rb'), )

        for i in range(len(neighbours)):
            users = map(lambda x: data.iloc[x]['username'], neighbours)
            c = Counter(reduce(lambda x, y: x + y, map(lambda x: user_mov_d[x], users)))
            neighbour_mov_cnt[data.iloc[i]['username']] = dict(c)
            for user in users:
                if not user in neighbour_mov_cnt:
                    neighbour_mov_cnt[user] = dict(c)



        if d[names_d['hasbooked']] or user_hasbooked[d[0]]:
            result = sorted([(k, neighbour_mov_cnt[d[0]][k]) for k in neighbour_mov_cnt[d[0]]], key=lambda x: -x[1])
        else:
            l = ['bengali', 'marathi', 'tamil', 'telugu', 'punjabi']
            lang = sorted([(i, int(d[names_d[i]])) for i in map(str.title, l)], key=lambda x: -x[1])
            toplang = lang[0][0] if lang[0][1] else 0
            gen_r = advertisement[d[names_d['gender']]]
            p_l, p_a, p_g, p_b = [], [], [], []
            p = []
            lang_r = []
            adv_r = []
            if toplang:
                lang_r = advertisement[toplang.lower()]
                p_l = [0.4]
            if d[d[names_d['adventure']]]:
                adv_r = advertisement['adventure']
                p_a = [0.2] * 3
            if p_l and p_a:
                p = [0.05, 0.2, 0.2, 0.2, 0.3, 0.025, 0.025]
            elif p_l:
                p = p_l + [0.35, 0.125, 0.125]
            elif p_a:
                p = p_a + [0.3, 0.05, 0.05]
            else:
                p = [0.6, 0.2, 0.2]
            if not gen_r:
                g = p.pop(0)
                inc = g / float(len(p))
                p = map(lambda x: x + inc, p)
            ban_r = advertisement['bank']
            rec = gen_r + lang_r + adv_r + ban_r
            p = self.get_random(len(rec), p=p)
            result = [d[names_d['username']]] + rec[int(p)]
        return result

    def post(self,request):
        recommend = self.get_recommendation(request.data)
        User.objects.create(username=request.data['username'], language=request.data['language'],
                                hasbooked = request.data['hasbooked'],location = request.data['location'])
        Movie.objects.create(username=request.data['name'],language=request.data['language'],genres=request.data['genres'])
        return Response({'recommend': recommend})