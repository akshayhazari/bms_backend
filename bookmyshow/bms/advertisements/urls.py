from django.conf.urls import url
from views import getRecommendations


urlpatterns = [
    url(r'get_recommendations/?$', getRecommendations.as_view(),name="get_recommendations")
 ]